import { useState } from "react";
import Counter from "./Counter";
import "./App.css";
import refreshImage from "./assets/arrows-rotate-solid.svg";
import restoreImage from "./assets/recycle-solid.svg";
import cartImage from "./assets/cart-shopping-solid.svg";

export default function App() {
  const [counters, setCounters] = useState([
    { id: 1, count: 0 },
    { id: 2, count: 0 },
    { id: 3, count: 0 },
    { id: 4, count: 0 },
  ]);

  const handleIncrement = (id) => {
    setCounters((prevState) =>
      prevState.map((counter) =>
        counter.id === id ? { ...counter, count: counter.count + 1 } : counter
      )
    );
  };

  const handleDecrement = (id) => {
    setCounters((prevState) =>
      prevState.map((counter) =>
        counter.id === id && counter.count > 0
          ? { ...counter, count: counter.count - 1 }
          : counter
      )
    );
  };

  const handleDelete = (id) => {
    setCounters((prevState) =>
      prevState.filter((counter) => counter.id !== id)
    );
  };

  const handleResetAll = () => {
    setCounters((prevState) =>
      prevState.map((counter) => ({ ...counter, count: 0 }))
    );
  };

  const handleRestoreAll = () => {
    if (counters.length === 0) {
      setCounters([
        { id: 1, count: 0 },
        { id: 2, count: 0 },
        { id: 3, count: 0 },
        { id: 4, count: 0 },
      ]);
    }
  };

  const nonZeroCounters = counters.filter((counter) => counter.count !== 0);

  return (
    <div className="main-container">
      <div className="head">
        <img src={cartImage} alt="" />
        <div className="head-display">{nonZeroCounters.length}</div>
        <div>Items</div>
      </div>
      <div className="reset-btn-container">
        <button onClick={handleResetAll} className="refresh-btn">
          <img src={refreshImage} alt="" />
        </button>
        <button
          onClick={handleRestoreAll}
          className="restore-btn"
        >
          <img src={restoreImage} alt="" />
        </button>
      </div>
      {
        <div className="counters-container">
          {counters.map((counter) => (
            <Counter
              key={counter.id}
              id={counter.id}
              count={counter.count}
              handleIncrement={handleIncrement}
              handleDecrement={handleDecrement}
              handleDelete={handleDelete}
            />
          ))}
        </div>
      }
    </div>
  );
}
